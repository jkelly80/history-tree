var clicked = null;

var myRoot = null;
var resp = null;
var modal = null;
//goto, delete, append
var bools = ["", null, false];
let currentPointer = null;
let clickedPointer = null;
var clickedPointerParent = null;
var tempAppendTree = null;
var tempAppendTreeParent = null;
var isActivatedPopup = false;

//This handles when the popup is opened
chrome.runtime.sendMessage({greeting: "hello"}, function(response) {
    console.log(response.farewell["root"]["children"].length);
    if (response.powerOn != null) {
        isActivatedPopup = response.powerOn;
    }
    if (response.catList != null) {
        categoryList = response.catList;
    }
    var a = response.farewell["root"];
    myRoot = a;
    currentPointer = response.farewell["current"];
    var blah = document.getElementById("here");
    blah.innerHTML = "";
    modal = document.getElementById('myModal');
    blah.appendChild(Tree.htmlBuilder(a, document,bools, modal, a));
    buildModal(document, modal);
    document.getElementById("powerButton").checked = isActivatedPopup;
    tabSelect("Tree");
    buildSecondModal();
    //sets the function for a click of the power button
    document.getElementById("powerButton").onclick = function() {
        changePowerStatus();
    };

    //sets the function for tab bar
    document.getElementById("treeButton").onclick = function() {
        tabSelect("Tree");
    }
    document.getElementById("catButton").onclick = function() {
        tabSelect("Categories");
        buildCats();
        dispCatItems();
    }

    //sets the function for the new category button
    document.getElementById("addCat").onclick = function() {
        showSecondModal();
        dispCatItems();
    }
});


//updates the power status on the background when toggle is changed
function changePowerStatus() {
    isActivatedPopup = document.getElementById("powerButton").checked;
    chrome.runtime.sendMessage({greeting: "powerChange",
                                power: isActivatedPopup});
}

//This dynamically builds the tree diagram
function buildPage() {
    var a = myRoot;
    var blah = document.getElementById("here");
    blah.innerHTML = "";

    modal = document.getElementById('myModal');
    blah.appendChild(Tree.htmlBuilder(a, document,bools, modal, a));
    buildModal(document, modal);
}

//This builds the modal but does not show it
function buildModal(doc, modal) {
    // Get the modal
    // Get the button that opens the modal
    // Get the <span> element that closes the modal
    var span = doc.getElementsByClassName("close")[0];
    var gotoButton = doc.getElementById("goto");
    var deleteButton = doc.getElementById("delete");
    var appendButton = doc.getElementById("append");
    var collButton = doc.getElementById("collapse");
    var chooseButton = doc.getElementById("chooseCat");
    var dropDown = doc.getElementById("catDropDown");

    gotoButton.onclick = function() {
        if (clickedPointer != null) {
            clickedPointer["notes"] = document.getElementById("notes").value;
            document.getElementById("notes").value = "";
            closeModal();
        }
        closeModal();
        bools[1] = false;
        bools[2] = null;
        chrome.runtime.sendMessage({url: bools[0],
                                    root: myRoot,
                                    power: isActivatedPopup});
    };

    //TODO rewrite this so that it actually deletes the element
    deleteButton.onclick = function() {
        bools[1].isVisible = false;
        buildPage();
        bools[0] = "";
        bools[1] = null;
        bools[2] = false;
        closeModal();
    }

    appendButton.onclick = function() {
        modal.style.display = "none";
        bools[0] = "";
        bools[1] = null;
        bools[2] = true;
        tempAppendTree = clickedPointer;
        tempAppendTreeParent = clickedPointerParent;
    }

    collButton.onclick = function() {
        var quickChilds = bools[1]["children"];
        for (var i = 0; i < quickChilds.length; i++) {
            quickChilds[i].isVisible = !quickChilds[i].isVisible;
        }
        buildPage();
        bools[0] = "";
        bools[1] = null;
        bools[2] = false;
        closeModal();
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        if (clickedPointer != null) {
            clickedPointer["notes"] = document.getElementById("notes").value;
            document.getElementById("notes").value = "";
            closeModal();
        }
//        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            if (clickedPointer != null) {
                clickedPointer["notes"] = document.getElementById("notes").value;
                document.getElementById("notes").value = "";
                closeModal();
            }
        }
    }

    //populate selector menu
    populateSelectorMenu();


}

function populateSelectorMenu() {
    deleteClass("selectorMenu");
    var dropDown = document.getElementById("catDropDown");
    for (var e = 0; e < Object.keys(categoryList).length; e++) {
        var op = document.createElement("option");
        op.setAttribute("class", "selectorMenu");
        op.innerText = Object.keys(categoryList)[e];
        dropDown.appendChild(op);
    }
}



//This handles a click on a node
function handleClick(tree, parent) {
    bools[0] = tree["data"];
    bools[1] = tree;
    if (bools[2] === true) {
        //append

        //First find and kill the tree in the parent
        for (var i = 0; i < tempAppendTreeParent["children"].length; i++) {
            if (tempAppendTree["data"] === tempAppendTreeParent["children"][i]["data"]) {
                tempAppendTreeParent["children"].splice(i, 1);
                break;
            }
        }
        //Now set tree to be in child array of the append node
        tree["children"].push(tempAppendTree);
        //Now update the background tree
        buildPage();
        closeModal();
    } else {
        //open the modal
        showModal(tree["notes"], tree);
    }
    for (var i = 2; i < bools.length; i++) {
        bools[i] = false;
    }
    clickedPointer = tree;
    clickedPointerParent = parent;
}

//This displays the modal and adds in the current notes
function showModal(note, clickedNode) {

    //Delete the left-over cats
    deleteClass("catItem");
    //load up the cats for this node
    for (var i = 0; i < clickedNode.category.length; i++) {
        var newLi = document.createElement("li");
        newLi.setAttribute("class", "catItem");
        var list = document.getElementById("catList");
        newLi.innerText = clickedNode.category[i];
        list.appendChild(newLi);
    }

    //set function for select button
    var chooseButton = document.getElementById("chooseCat");
    var dropDown = document.getElementById("catDropDown");
    chooseButton.onclick = function() {
        var newEl = document.createElement("li");
        newEl.setAttribute("class", "catItem");
        var catList = document.getElementById("catList");
        var catName = dropDown.options[dropDown.selectedIndex].text;
        newEl.innerText = catName;
        catList.appendChild(newEl);
        clickedNode.category.push(catName);
    };

    modal.style.display = "block";
    document.getElementById("notes").value = note;
}

//Closes the modal and sends the new tree back to the the background
function closeModal() {
    modal.style.display = "none";
    chrome.runtime.sendMessage({root: myRoot});
}

function tabSelect(tabName) {
    var i, tabContent, tabLinks;

    tabContent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabContent.length;i++) {
        tabContent[i].style.display = "none";
    }

    document.getElementById(tabName).style.display = "block";
}




//Here begins the portion of the code that power the categories feature
//I have decided to go through the entire tree each time the category
//feature is queried in order to be the most efficient

var categoryList = {"Uncategorized":[]}

//creates the second modal
function buildSecondModal() {
    var span = document.getElementsByClassName("secondClose");
    span[0].onclick = function() {
        closeSecondModal();
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            var secondModal = document.getElementById("catModal");
            secondModal.style.display = "none";
        }
    }
}

function showSecondModal() {
    var secondModal = document.getElementById("catModal");
    secondModal.style.display = "block";
}

function closeSecondModal() {
    var name = document.getElementById("catName").value;
    categoryList[name] = [];
    var secondModal = document.getElementById("catModal");
    secondModal.style.display = "none";
    chrome.runtime.sendMessage({
        greeting: "catUpdate",
        catList: categoryList
    });
    dispCatItems();
    populateSelectorMenu();
}

//sorts things into their respective categories
function buildCats() {
    for (var i = 0; i < Object.keys(categoryList).length; i++) {
        categoryList[Object.keys(categoryList)[i]] = [];
    }
    buildCatsHelper(myRoot);
}

function buildCatsHelper(node) {
    //go through the nodes categories and then assign them
    if (node.category.length > 0) {
        for (var i = 0; i < node.category.length;i++) {
            categoryList[node.category[i]].push({
                "data": node.data,
                "notes": node.notes
            });
        }
    } else {
        categoryList["Uncategorized"].push({
            "data": node.data,
            "notes": node.notes
        });
    }

    //recurse down
    for (var i = 0; i < node.children.length; i++) {
        buildCatsHelper(node.children[i]);
    }
}

function dispCatItems() {
    deleteClass("catedItemsClass");
    var list = document.getElementById("catedItems");
    for (var i = 0; i < Object.keys(categoryList).length; i++) {
        var li = document.createElement("li");
        li.setAttribute("class", "catedItemsClass");
        var ul = document.createElement("ul");
        var title = document.createElement("button");
        var catNameString = Object.keys(categoryList)[i];
        title.style.border = "black";
        title.style.display = "inline - block";
        title.id = "button";
        title.style.background = "#f44336";
        title.style.padding = "15px 32px"
        title.style.fontsize = "16px";
        title.style.color = "white";
        title.href = catNameString;
        title.innerHTML = catNameString;
        //catNameLabel.innerText = catNameString;
        li.appendChild(title);
        for (var j = 0; j < categoryList[catNameString].length; j++) {
            var but = document.createElement("button");
            var child = document.createElement("li");
            but.style.border = "back";
            but.style.display = "inline-block";
            but.style.background = "#f44336";
            but.style.padding = "15px 32px";
            but.style.fontsize = "16px";
            but.style.color = "white";
            but.innerText = categoryList[catNameString][j].data;
            child.appendChild(but);
            ul.appendChild(child);
        }
        li.appendChild(ul);
        list.appendChild(li);
    }
}

function deleteClass(className) {
    var leftOvers = document.getElementsByClassName(className);
    for (var i = 0; i < leftOvers.length; i++) {
        leftOvers[i].parentNode.removeChild(leftOvers[i]);
        i--;
    }
}

function buildThirdModal() {
    var span = document.getElementsByClassName("secondClose");
    span[1].onclick = function() {
        closeThirdModal();
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            var thirdModal = document.getElementById("thirdModal");
            thirdModal.style.display = "none";
        }
    }
}

function closeThirdModal() {

}
