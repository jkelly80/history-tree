class Tree {

    root;
    current;

    constructor(root) {
        this.root = root;
        this.current = root;
    }

    static append(the, subRoot) {
        subRoot["children"].push(the.root);
    }

    static findNode(the, data) { //68, 52
        return Tree.findNodeHelper(data, the.root);
    }

    static findNodeHelper(findData, startNode) {
        if (startNode["data"] === findData) {
            return startNode;
        }
        var children = startNode.children;
        for (var i = 0; i < children.length; i++) {
            if (children[i]["data"] === findData) {
                return children[i];
            } else {
                var temp = Tree.findNodeHelper(findData, children[i]);
                if (temp != null) {
                    return temp;
                }
            }
        }
        return null;
    }

    static contains(the, data) { //40
        if (the == undefined) {
            return false;
        }
        return Tree.containsHelper(data, the.root);
    }

    static containsHelper(findData, startNode) {
        var children = startNode.children;
        if (startNode["data"] === findData) {
            return true;
        }
        for (var i = 0; i < children.length; i++) {
            console.log(children[i]);
            console.log(findData);
            if (children[i]["data"] === findData) {
                return true;
            } else {
                if (Tree.containsHelper(findData, children[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    toJson() {
        return JSON.stringify(this);
    }

    static toObject(json) {
        return JSON.parse(json);
    }

    static treePrinter(tree) {
        console.log(tree["data"]);
        var child = tree["children"];
        for (var i = 0; i < child.length; i++) {
            Tree.treePrinter(child[i]);
        }
    }

    static htmlBuilder(tree, doc,bools, modal, parental) {
        var block = doc.createElement("div");
        var title = doc.createElement("Button");
        var str = tree["data"];
        var boxLim = 40;
        if (str != "Root") {
            if (str.length > boxLim) {
                title.href = str;
                title.innerHTML = str.substring(0, boxLim);
            } else {
                title.href = str;
                title.innerHTML = str;
            }
        }
        title.style.border = "black";
        title.style.display = "inline - block";
        title.id = "button";
        title.style.background = "#f44336";
        title.style.padding = "15px 32px"
        title.style.fontsize = "16px";
        title.style.color = "white";
        title.onclick = function() {
            handleClick(tree, parental);
        };
        var list = doc.createElement("ul");
        list.style.listSytleType = "none";
        var child = tree["children"];
        for (var i = 0; i < child.length; i++) {
            if (child[i].isVisible === true) {
                var item = doc.createElement("li");
                var inside = Tree.htmlBuilder(child[i], doc,bools, modal, tree);
                item.appendChild(inside);
                list.appendChild(item);
            }
        }
        block.appendChild(title);
        block.appendChild(list);
        return block;
    }

}

class TreeNode {

    data;
    isVisible;
    children = [];
    simpleName;
    notes = "";
    category = [];

    constructor(data) {
        this.data = data;
        this.isVisible = true;
        this.simpleName = "";
    }

    addChild(child) {
        this.children.push(child);
    }

    getChild(index) {
        return this.children[index];
    }

    getData() {
        return this.data;
    }

    equals(a) {
        return this.data == a.getData();
    }

    equalsData(data) {
        return this.data == data;
    }

    toHTML(doc) {
        var div = doc.createElement("div");
        var link = doc.createElement("a");
        link.setAttribute("href", this.data);
        link.textContent = this.data;
        div.textContent = link;
        for (var i = 0; i < this.children.length; i++) {
            div.appendChild(children[i].toHTML(doc));
        }
        return div;
    }

}
