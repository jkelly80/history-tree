// background.js

// Called when the user clicks on the browser action.
var root = new TreeNode("Root");
var tree = new Tree(root);
let currentPointer = tree["current"];
var isNewTab = false;
var isActivatedBackground = true;

//this holds all of the categories that the user has created
var categoryList = {"Uncategorized":[]}

//I'm not totally sure whats this does
chrome.browserAction.onClicked.addListener(function(tab) {
    /*
    chrome.browserAction.setPopup({popup: "popup.html"}, function() {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            var activeTab = tabs[0];
            chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_browser_action"});
        });
    });
*/
});

//This handles when the popup is opened
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.greeting == "hello") {
            sendResponse({farewell: tree,
                          powerOn: isActivatedBackground,
                          catList: categoryList});
        } else if (request.greeting == "powerChange") {
            isActivatedBackground = request.power;
        } else if (request.greeting == "catUpdate") {
            categoryList = request.catList;
        } else if (request.url != undefined) {
            chrome.tabs.create({ url: request.url });
            root = request.root;
            tree = new Tree(root);
        } else {
            root = request.root;
            tree = new Tree(root);
        }
    });

//This logs when a tab has been created. It adds it to the local copy of the tree
chrome.tabs.onCreated.addListener(function(tab) {
    if (isActivatedBackground) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            var activeTab = tabs[0];
            var tempNode = new TreeNode(activeTab.url);
            if (!currentPointer["data"] === (activeTab.url) || !Tree.contains(tree, activeTab.url)) {
                root["children"].push(tempNode);
                currentPointer = tempNode;
                tree["current"] = currentPointer;
            }
        });
    }
});

//This updates the tree when the url of the current tab changes
chrome.tabs.onUpdated.addListener(function(tab) {
    if (isActivatedBackground) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            var activeTab = tabs[0];
            var tempNode = new TreeNode(activeTab.url);
            var findNode = Tree.findNode(tree, activeTab.url);
            if (currentPointer["data"] != activeTab.url && findNode == null) {
                currentPointer["children"].push(tempNode);
                currentPointer = tempNode;
                tree["current"] = currentPointer;
            } else if (findNode != null) {
                currentPointer = findNode;
                tree["current"] = currentPointer;
            }
        });
    }
});

//This changes the current pointer (the thing that monitors where the user is
//so that children can be correctly added) to be the new tab
chrome.tabs.onActivated.addListener(function(tab) {
    if (isActivatedBackground) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            var activeTab = tabs[0];
            var url = activeTab.url;
            var temp = Tree.findNode(tree, url);
            if (temp === null) {
                temp = new TreeNode(url);
                root["children"].push(temp);
            }
            currentPointer = temp;
            tree["current"] = currentPointer;
        });
    }
});
